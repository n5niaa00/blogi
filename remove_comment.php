<?php include_once 'inc/top.php';?>
    <div class="container">
        <div class="starter-template">
        <?php
        if (isset($_SESSION['kayttaja_id'])) {
            $kirjoitus_id = filter_input(INPUT_GET,'kirjoitus_id',FILTER_SANITIZE_NUMBER_INT);
            $kommentti_id = filter_input(INPUT_GET,'kommentti_id',FILTER_SANITIZE_NUMBER_INT);
            
            $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');

            $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
                    
            try {
                $kysely = $tietokanta->prepare("DELETE FROM kommentti WHERE id=:kommentti_id AND kirjoitus_id=:kirjoitus_id");
                
                $kysely->bindValue(':kommentti_id', $kommentti_id, PDO::PARAM_INT);
                $kysely->bindValue(':kirjoitus_id', $kirjoitus_id, PDO::PARAM_INT);
                
                if ($kysely->execute()) {
                    print('<p>Kommentti poistettu.</p>');
                }
                else {
                    print '<p>';
                    print_r($tietokanta->errorInfo());
                    print '</p>';
                }
                print '<a href="post.php?id=' . $kirjoitus_id . '">Takaisin kirjoitukseen</a>';
                
            } catch (PDOException $pdoex) { 
                print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
            }
        }
        else {
            print '<p>Kommenttien poistaminen on mahdollista vain sisään kirjautuneille käyttäjille.</p>';
            print("<a href='index.php'>Takaisin etusivulle</a>");
        }
        ?>
        </div>
    </div>
<?php include_once 'inc/bottom.php';?>