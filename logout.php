<?php include_once 'inc/top.php';?>
<div class="container">
    <?php
        if (isset($_SESSION['kayttaja_id'])) {
            unset($_SESSION['kayttaja_id']);
        }
        header('Location: index.php');
    ?>
</div>
<?php include_once 'inc/bottom.php';?>