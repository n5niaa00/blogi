<?php include_once 'inc/top.php';?>
    <div class="container">
        <div class="tekstit">
        <a href="index.php">Takaisin etusivulle</a>
        <?php
            $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
            $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            
            $kirjoitus_id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
            
            if ($_SERVER['REQUEST_METHOD']==='POST') {
                $kirjoitus_id = filter_input(INPUT_POST,'kirjoitus_id',FILTER_SANITIZE_NUMBER_INT);
                
                try {
                
                $kommentti = filter_input(INPUT_POST,'kommentti',FILTER_SANITIZE_STRING);

                $kysely = $tietokanta->prepare("INSERT INTO kommentti (teksti, kirjoitus_id, kayttaja_id) VALUES (:kommentti, :kirjoitus_id, :kayttaja_id)");

                $kysely->bindValue(':kommentti',$kommentti,PDO::PARAM_STR);
                $kysely->bindValue(':kirjoitus_id',$kirjoitus_id,PDO::PARAM_STR);
                $kysely->bindValue(':kayttaja_id',$_SESSION['kayttaja_id'],PDO::PARAM_STR);

                if ($kysely->execute()) {
                    print('<p>Kommentti tallennettu</p>');
                    header("Location: post.php?id=$kirjoitus_id");
                }
                else {
                    print '<p>';
                    print_r($tietokanta->errorInfo());
                    print '<p>';
                }
                
                } catch (PDOException $pdoex) {
                    print '<p>Kommentin postittaminen epäonnistui.' . $pdoex->getMessage(). '</p>';
                }
            }
            
            try {
                
                $sql_kirjoitus = 'SELECT * FROM kirjoitus INNER JOIN kayttaja ON kirjoitus.kayttaja_id = kayttaja.id WHERE kirjoitus.id =' . $kirjoitus_id;
                $kysely_kirjoitus = $tietokanta->query($sql_kirjoitus);
                
                if($kysely_kirjoitus) {
                    $tietue = $kysely_kirjoitus->fetch();
                    
                    print '<h1>' . $tietue['otsikko'] . '</h1>';
                    print '<p>' . date('d.m.Y H.i',strtotime($tietue['paivays'])) . ' by ' . $tietue['tunnus'] . '</p>';
                    print '<p>' . $tietue['teksti'] . '</p>';
                    
                    if (isset($_SESSION['kayttaja_id'])) {
                    ?>
                    <h3>Kommentit</h3>
                    <form id="lisaa_kommentti" method="post" action="<?php print($_SERVER['PHP_SELF']);?>">
                        <input type="text" name="kirjoitus_id" value="<?php print $kirjoitus_id;?>">
                        <textarea  class="form-control" name="kommentti" id="kommentti"></textarea><br>
                        <input type="submit" class="btn btn-primary" value="Lisää kommentti">
                    </form>
                    <?php
                    }
                    $sql_kommentti = 'SELECT *,kommentti.id AS kommentti_id FROM kommentti INNER JOIN kayttaja ON kommentti.kayttaja_id = kayttaja.id WHERE kirjoitus_id = ' . $kirjoitus_id . ' ORDER BY paivays desc';
                    $kysely_kommentti = $tietokanta->query($sql_kommentti);
                    
                    if ($kysely_kommentti)
                    {   print '<div class="tekstit">';
                        print '<ul>';
                        while ($tietue = $kysely_kommentti->fetch()) {
                            print '<li>';
                            print $tietue['teksti'] . ' ' . date('d.m.Y H.i',strtotime($tietue['paivays'])) . ' by ' . $tietue['tunnus'];
                            if (isset($_SESSION['kayttaja_id'])) {
                                print '&nbsp; <a href="remove_comment.php?kommentti_id=' . $tietue['kommentti_id'] . '&kirjoitus_id=' . $tietue['kirjoitus_id'] . '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
                            }
                            print '</li>';
                        }
                        print '</ul>';
                        print '</div>';
                    }
                    else {
                        print '<p>';
                        print_r($tietokanta->errorInfo());
                        print '</p>';
                    }
                }
                else {
                    print '<p>';
                    print_r($tietokanta->errorInfo());
                    print '</p>';
                }
            } catch (PDOException $pdoex) {
                print '<p>Tietokannan avaus epäonnistui ' . $pdoex->getMessage() .'</p>';
            }
        ?>
        
        </div>
    </div><!-- /.container -->
<?php include_once 'inc/bottom.php';?>