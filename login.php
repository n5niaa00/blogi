<?php include_once 'inc/top.php';?>
<div class="container"
<?php
$viesti = "";

$tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
$tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if ($tietokanta!=null) {
        try {
            $tunnus = filter_input(INPUT_POST,'tunnus',FILTER_SANITIZE_STRING);
            $salasana = md5(filter_input(INPUT_POST,'salasana',FILTER_SANITIZE_STRING));
            
            $sql="SELECT * FROM kayttaja WHERE tunnus='$tunnus' AND salasana='$salasana'";
            
            $kysely = $tietokanta->query($sql);
            
            if ($kysely->rowCount()===1) {
                $tietue = $kysely->fetch();
                $_SESSION['login'] = true;
                $_SESSION['kayttaja_id'] = $tietue['id'];
                header('Location: index.php');
            }
            else {
                $viesti = "Väärä tunnus tai salasana!";
            }
        } catch (PDOException $pdoex) {
            print "Käyttäjän tietojen hakeminen epäonnistui." . $pdoex->getMessage();
        }
    }
}
?>
     <div class="starter-template">
         <h3 style="text-align: left">Kirjaudu sisään</h3>
         <form action="<?php print($_SERVER['PHP_SELF']);?>" method="post">
             <div class="form-group">
                 <label>Tunnus</label>
                 <input type="text" name="tunnus" class="form-control">
             </div>
             <div class="form-group">
                 <label>Salasana</label>
                 <input type="password" name="salasana" class="form-control">
             </div>
             <div class="form-group">
                 <input type="submit" class="btn btn-default" value="Kirjaudu">
             </div>
         </form>
         <p><?php print($viesti);?></p>
        
    </div>
</div>
<?php include_once 'inc/bottom.php';?>
