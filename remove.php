<?php include_once 'inc/top.php';?>
    <div class="container">
        <div class="starter-template">
        <?php
        if (isset($_SESSION['kayttaja_id'])) {
            $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
        
            $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
            
            $tietokanta->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            
            try {
                
                $kommentti_kysely = $tietokanta->prepare("DELETE FROM kommentti WHERE kirjoitus_id=:id");
                $kirjoitus_kysely = $tietokanta->prepare("DELETE FROM kirjoitus WHERE id=:id");
                $kommentti_kysely->bindValue(':id', $id, PDO::PARAM_INT);
                $kirjoitus_kysely->bindValue(':id', $id, PDO::PARAM_INT);
                
                if ($kommentti_kysely->execute()) {
                    
                    if ($kirjoitus_kysely->execute()) {
                        print('<p>Kirjoitus poistettu.</p>');
                    }
                    else {
                        print '<p>';
                        print 'Kirjoituksen poisto epäonnistui:';
                        print_r($tietokanta->errorInfo());
                        print '</p>';
                    }
                    print("<a href='index.php'>Takaisin etusivulle</a>");
                }
                else {
                    print '<p>';
                    print 'Kommenttien poisto epäonnistui:';
                    print_r($tietokanta->errorInfo());
                    print '</p>';
                }   
            } catch (PDOException $pdoex) { 
                print '<p>Kirjoituksen poisto epäonnistui.' . $pdoex->getMessage(). '</p>';
            }
        }
        else {
            print '<p>Kirjoitusten poistaminen on mahdollista vain sisään kirjautuneille käyttäjille.</p>';
            print("<a href='index.php'>Takaisin etusivulle</a>");
        }
        ?>
        </div>
    </div>
<?php include_once 'inc/bottom.php';?>