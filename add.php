<?php include_once 'inc/top.php';?>
    <div class="container">
      <div class="starter-template">
    <?php
        $kayttaja_id=1;
        $otsikko="";
        $teksti="";
        
        $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
        
        $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        if ($_SERVER['REQUEST_METHOD']==='POST') {
            try {
                $otsikko = filter_input(INPUT_POST,'otsikko',FILTER_SANITIZE_STRING);
                $teksti = filter_input(INPUT_POST,'teksti',FILTER_SANITIZE_STRING);
                
                $kysely = $tietokanta->prepare("INSERT INTO kirjoitus (otsikko,teksti,kayttaja_id) VALUES (:otsikko,:teksti,:kayttaja_id)");
                
                $kysely->bindValue(':otsikko',$otsikko,PDO::PARAM_STR);
                $kysely->bindValue(':teksti',$teksti,PDO::PARAM_STR);
                $kysely->bindValue(':kayttaja_id',$kayttaja_id,PDO::PARAM_INT);
                
                if ($kysely->execute()) {
                    print('<p>Kirjoitus tallennettu</p>');
                }
                else {
                    print '<p>';
                    print_r($tietokanta->errorInfo());
                    print '</p>';
                }
            } catch (PDOException $pdoex) {
                print '<p>Tietokannan avaus-epäonnistui.' . $pdoex->getMessage(). '</p>';
            }
        }
    ?>
          <h3 style="text-align: left">Lisää kirjoitus</h3>
          <form action="<?php print($_SERVER['PHP_SELF']);?>" method="post">
              <div class="form-group">
                  <label>Otsikko</label>
                  <input type="text" name="otsikko" class="form-control" placeholder="Otsikko tähän">
              </div>
              <div class="form-group">
                  <label>Teksti</label>
                  <textarea name="teksti" class="form-control" rows="3" placeholder="Teksti tänne..."></textarea>
              </div>
              <div class="form-group">
                  <input type="submit" class="btn btn-primary" value="Tallenna">
                  <input type="button" class="btn btn-default" value="Peruuta" onclick="window.location ='index.php';" >
              </div>
          </form>
      </div>

    </div>
<?php include_once 'inc/bottom.php';?>