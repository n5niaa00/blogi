<?php include_once 'inc/top.php';?>
    <div class="container">
      
        <div class="tekstit">
        <?php
            $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');

            $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
            try {

                //$sql = 'SELECT * FROM kirjoitus ORDER BY paivays';
                $sql = "SELECT *,kirjoitus.id as id FROM kirjoitus INNER JOIN kayttaja ON kirjoitus.kayttaja_id = kayttaja.id ORDER BY paivays desc";

                $kysely = $tietokanta->query($sql);

                if($kysely) {
                    while ($tietue = $kysely->fetch()) {
                        print '<div class="kirjoitus">';
                        print '<p>';
                        print date('d.m.Y H.i',strtotime($tietue['paivays'])) . ' by ' . $tietue['tunnus'] . '<br />';
                        print '&nbsp; <a href="post.php?id=' . $tietue['id'] .  'style="font-weight:bold;">' . $tietue['otsikko'] . '</a>';
                        //print $tietue['teksti'];
                        if (isset($_SESSION['kayttaja_id'])) {
                             print '&nbsp; <a href="remove.php?id=' . $tietue['id'] .'"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
                        }
                        print '</p>';
                        print '<hr>';
                        print '</div>';
                    }
                }

            } catch (PDOException $pdoex) {
                print '<p>Tietokannan avaus epäonnistui ' . $pdoex->getMessage() .'</p>';
            }
        ?>
        </div>
    </div><!-- /.container -->
<?php include_once 'inc/bottom.php';?>